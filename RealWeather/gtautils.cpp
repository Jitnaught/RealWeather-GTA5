#include "script.h"
#include "gtautils.h"

char* weatherNames[] = { "EXTRASUNNY", "CLEAR", "CLOUDS", "SMOG", "FOGGY", "OVERCAST", "RAIN", "THUNDER", "CLEARING", "NEUTRAL", "SNOW", "BLIZZARD", "SNOWLIGHT", "XMAS", "HALLOWEEN" };

void subtitle(char *message, int duration)
{
	UI::BEGIN_TEXT_COMMAND_PRINT("CELL_EMAIL_BCON");
	UI::ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(message);
	UI::END_TEXT_COMMAND_PRINT(duration, true);
}

int notify(char *message, bool blinking)
{
	UI::_SET_NOTIFICATION_TEXT_ENTRY("STRING");
	UI::ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(message);

	return UI::_DRAW_NOTIFICATION(blinking, 1);
}

char *getCurrentWeather()
{
	char *weather = "";

	for (int i = 0; i < sizeof(weatherNames) / sizeof(weatherNames[0]); i++)
	{
		if (GAMEPLAY::GET_HASH_KEY(weatherNames[i]) == GAMEPLAY::GET_PREV_WEATHER_TYPE_HASH_NAME())
		{
			weather = weatherNames[i];
			break;
		}
	}

	return weather;
}
