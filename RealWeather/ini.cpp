#include "ini.h"

std::string INI::getString(LPCSTR section, LPCSTR key, LPCSTR default)
{
	char temp[1024];
	int result = GetPrivateProfileString(section, key, default, temp, sizeof(temp), INI::filename);
	return std::string(temp, result);
}

int INI::getInt(LPCSTR section, LPCSTR key, int default)
{
	return GetPrivateProfileInt(section, key, default, INI::filename);
}