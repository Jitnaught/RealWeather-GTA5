#include "utils.h"

#include <Windows.h>
#include <iomanip> //for time
#include <cctype> //for std::isspace
#include <sstream> //for std::stringstream

bool firstLog = true;

bool isKeyPressed(int nVirtKey)
{
	return (GetAsyncKeyState(nVirtKey) & 0x8000) != 0;
}

bool stringContains(const std::string& base, const std::string& search)
{
	return base.find(search) != std::string::npos;
}

bool stringOnlyWhitespace(const std::string& str)
{
	auto it = str.begin();

	do 
	{
		if (it == str.end()) return true;
	} while (*it >= 0 && *it <= 0x7f && std::isspace(*(it++)));

	//one of these conditions will be optimized away by the compiler,
	//which one depends on whether char is signed or not

	return false;
}

std::string getDateTimeNowString()
{
	time_t t = time(0);
	struct tm now;
	localtime_s(&now, &t);

	std::stringstream s;
	s << (now.tm_year + 1900) << '-'
		<< (now.tm_mon + 1) << '-'
		<< now.tm_mday << " "
		<< now.tm_hour << ":"
		<< now.tm_min << ":"
		<< now.tm_sec;

	return s.str();
}
