#pragma once

void subtitle(char *message, int duration = 2500);
int notify(char *message, bool blinking = false);
char *getCurrentWeather();