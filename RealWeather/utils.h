#pragma once

#include <string>

bool isKeyPressed(int nVirtKey);
bool stringContains(const std::string& base, const std::string& search);
bool stringOnlyWhitespace(const std::string& str);
std::string getDateTimeNowString();