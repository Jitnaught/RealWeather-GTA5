#pragma once

#include <Windows.h>
#include <string>

class INI
{
	LPCSTR filename;

public:
	INI(LPCSTR file) { filename = file; }
	std::string getString(LPCSTR section, LPCSTR key, LPCSTR default);
	int getInt(LPCSTR section, LPCSTR key, int default);
};