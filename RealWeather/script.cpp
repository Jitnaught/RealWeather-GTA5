#include "script.h"
#include "utils.h"
#include "gtautils.h"
#include "web.h"
#include "ini.h"
#include "tinyxml2.h"

#include <Windows.h>
#include <iostream> //for std::clog
#include <fstream> //for ofstream
#include <process.h> //for _beginthreadex

const char *error = "error";
const char *defaultApiKey = "&mode=xml&APPID=95e1e267fa5e57d5fc9acd10e91bcebe";

bool debug, notifyWhenSetWeather;
int holdKey, pressKey;
std::string weatherUrl;

bool wasPressKeyPressed = false;

bool enabled;
char *weatherToSetTo = const_cast<char*>(error), *lastWeatherToSetTo = weatherToSetTo;

unsigned int __stdcall getWeather(void* data)
{
	std::string content = downloadString(weatherUrl);

	if (content != "")
	{
		if (debug) std::clog << "Weather API response: " << content << std::endl;

		if (stringContains(content, "<current>")) //no error response
		{
			tinyxml2::XMLDocument doc;
			doc.Parse(content.c_str());

			int weatherNum = std::stoi(doc.FirstChildElement("current")->FirstChildElement("weather")->Attribute("number"));

			char *weatherName = const_cast<char*>(error);

			switch (weatherNum)
			{
			case 200: //thunderstorm light rain
			case 201: //thunderstorm with rain
			case 202: //thunderstorm with heavy rain
			case 210: //light thunderstorm
			case 211: //thunderstorm
			case 212: //heavy thunderstorm
			case 221: //ragged thunderstorm
			case 230: //thunderstorm with light drizzle
			case 231: //thunderstorm with drizzle
			case 232: //thunderstorm with heavy drizzle
			case 960: //storm
			case 961: //violent storm
				weatherName = "THUNDER";
				break;
			case 300: //light intensity drizzle
			case 301: //drizzle
			case 302: //heavy intensity drizzle
			case 310: //light intensity drizzle rain
			case 311: //drizzle rain
			case 312: //heavy intensity drizzle rain
			case 313: //shower rain and drizzle
			case 314: //heavy shower rain and drizzle
			case 321: //shower drizzle
			case 500: //light rain
			case 501: //moderate rain
			case 502: //heavy intensity rain
			case 503: //very heavy rain
			case 504: //extreme rain
			case 511: //freezing rain
			case 520: //light intensity shower rain
			case 521: //shower rain
			case 522: //heavy intensity shower rain
			case 531: //ragged shower rain
				weatherName = "RAIN";
				break;
			case 600: //light snow
			case 615: //light rain and snow
			case 620: //light shower snow
				weatherName = "SNOWLIGHT";
				break;
			case 601: //snow
			case 611: //sleet
			case 612: //shower sleet
			case 616: //rain and snow
			case 621: //shower snow
				weatherName = "SNOW";
				break;
			case 602: //heavy snow
			case 622: //heavy shower snow
				weatherName = "BLIZZARD";
				break;

			case 762: //volcanic ash
				weatherName = "FOGGY"; //idk...
				break;
			case 781: //tornado
			case 900:
				weatherName = "THUNDER";
				break;
			case 800: //clear sky
				weatherName = "EXTRASUNNY";
				break;
			case 801: //few-
			case 802: //scattered-
			case 803: //broken-
				weatherName = "CLOUDS";
				break;
			case 804: //overcast clouds
				weatherName = "OVERCAST";
				break;
			case 901: //tropical storm
				weatherName = "THUNDER";
				break;
			case 902: //hurricane
			case 962:
				weatherName = "THUNDER";
				break;
			case 741: //fog
				weatherName = "FOGGY";
				break;

			case 701: //mist
			case 711: //smoke
			case 721: //haze
				weatherName = "SMOG";
				break;
			}

			weatherToSetTo = weatherName;

			if (debug && weatherToSetTo != error) std::clog << "Successfully retrieved and parsed weather: " << std::string(weatherName) << std::endl;
		}
		else
		{
			std::string errorMessage = "openweathermap.org did not return weather data. Returned string: " + content;

			if (stringContains(content, "Invalid API key"))
			{
				errorMessage = "Invalid API key.";
			}
			else if (stringContains(content, "Error: Not found city"))
			{
				errorMessage = "openweathermap.org did not find the desired location, please check for spelling errors.";
			}
			else if (stringContains(content, "404"))
			{
				errorMessage = "openweathermap.org error 404.";
			}
			else if (stringContains(content, "500 /proxy/data/2.5/"))
			{
				errorMessage = "openweathermap.org internal server error 500.";
			}
			else if (stringContains(content, "401"))
			{
				errorMessage = "Too many requests to API key, please sign up for openweathermap.org, go to home.openweathermap.org, copy the API key, and then paste it into RealWeather.ini after \"API_KEY = \".";
			}

			std::clog << "Error: " << errorMessage << std::endl;
			notify(const_cast<char*>(errorMessage.c_str()), true);
		}
	}

	return 0;
}

void main()
{
	INI ini(".\\RealWeather.ini");

	enabled = ini.getInt("SETTINGS", "ENABLED_ON_STARTUP", 1) != 0;
	debug = ini.getInt("SETTINGS", "DEBUG", 0) != 0;
	notifyWhenSetWeather = ini.getInt("SETTINGS", "NOTIFY_WHEN_SET_WEATHER", 0) != 0;
	std::string location = ini.getString("SETTINGS", "LOCATION", "Chicago, IL");
	std::string apiKey = ini.getString("SETTINGS", "API_KEY", "");
	holdKey = std::strtol(ini.getString("KEYS", "HOLD_KEY", "0xA3").c_str(), NULL, 0);
	pressKey = std::strtol(ini.getString("KEYS", "PRESS_KEY", "0x57").c_str(), NULL, 0);

	if (apiKey != "" && !stringOnlyWhitespace(apiKey)) apiKey = "&mode=xml&APPID=" + apiKey;
	else apiKey = defaultApiKey;

	weatherUrl = "http://api.openweathermap.org/data/2.5/weather?q=" + location + apiKey;

	std::ofstream out(".\\RealWeather.log", std::ios_base::out | std::ios_base::app);
	auto old_rdbuf = std::clog.rdbuf(); //save old rgbuf for resetting to later
	std::clog.rdbuf(out.rdbuf()); //set clog rdbuf to output to log file

	if (debug)
	{
		std::clog << std::endl << getDateTimeNowString() << std::endl << "--------------------" << std::endl;

		std::clog << "Weather URL: " << weatherUrl << std::endl;
	}

	DWORD lastTimeWeatherRan = 0;

	while (true)
	{
		if (isKeyPressed(pressKey))
		{
			wasPressKeyPressed = true;
		}
		else if (wasPressKeyPressed)
		{
			wasPressKeyPressed = false;

			if (holdKey == -1 || isKeyPressed(holdKey))
			{
				enabled = !enabled;
				lastTimeWeatherRan = 0;

				std::string toggleMessage = "RealWeather " + std::string(enabled ? "enabled" : "disabled");
				if (debug) std::clog << toggleMessage << std::endl;
				subtitle(const_cast<char*>((toggleMessage).c_str()));
			}
		}

		if (enabled && !DLC2::GET_IS_LOADING_SCREEN_ACTIVE() && (CAM::IS_SCREEN_FADED_IN() || CAM::IS_SCREEN_FADING_IN()))
		{
			if (weatherToSetTo != error)
			{
				bool realWeatherChanged = weatherToSetTo != lastWeatherToSetTo;
				bool gameWeatherChanged = !GAMEPLAY::IS_NEXT_WEATHER_TYPE(weatherToSetTo);

				if (realWeatherChanged || gameWeatherChanged)
				{
					lastWeatherToSetTo = weatherToSetTo;
					
					char *originalWeather = getCurrentWeather();

					GAMEPLAY::CLEAR_WEATHER_TYPE_PERSIST();
					GAMEPLAY::CLEAR_OVERRIDE_WEATHER();

					if (debug) std::clog << "Set weather to " + std::string(weatherToSetTo) << std::endl;

					if (realWeatherChanged)
					{
						if (notifyWhenSetWeather) notify(const_cast<char*>(("Transitioning to " + std::string(weatherToSetTo)).c_str()));

						GAMEPLAY::SET_WEATHER_TYPE_NOW(originalWeather);
						GAMEPLAY::_SET_WEATHER_TYPE_OVER_TIME(weatherToSetTo, 30.0f); //transition over 10 seconds
						
						DWORD startTickCount = GetTickCount();

						while (!GAMEPLAY::IS_PREV_WEATHER_TYPE(weatherToSetTo) && GetTickCount() - startTickCount < 11000) //wait until transition over
						{
							WAIT(1);
						}

						GAMEPLAY::SET_WEATHER_TYPE_NOW_PERSIST(weatherToSetTo);
					}
					else if (gameWeatherChanged)
					{
						GAMEPLAY::SET_WEATHER_TYPE_NOW_PERSIST(weatherToSetTo);
					}
				}
			}

			DWORD tickCount = GetTickCount();

			if (lastTimeWeatherRan + 60000 <= tickCount)
			{
				lastTimeWeatherRan = tickCount;

				_beginthreadex(NULL, 0, &getWeather, NULL, 0, NULL);
			}
		}

		WAIT(5);
	}

	std::clog.rdbuf(old_rdbuf);
	out.flush();
	out.close();
}

void ScriptMain()
{
	srand(GetTickCount());
	main();
}
