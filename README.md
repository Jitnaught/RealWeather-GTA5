RealWeather mod for GTA V.

Installation requirements: Script Hook V and [Visual C++ 2015 Redistributable](https://www.microsoft.com/en-us/download/details.aspx?id=52685).

Compilation requirements: Script Hook V SDK and Visual Studio 2015 (or 2017+ with [Visual C++ 2015 Build Tools](https://landinghub.visualstudio.com/visual-cpp-build-tools) installed).

Uses libcurl to get weather data and TinyXML-2 to parse the data.